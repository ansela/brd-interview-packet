import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'font-awesome/css/font-awesome.css';

import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import AuthenticationService from './services/authentication/AuthenticationService';

export default class App extends React.Component {

  services = {
    authenticationService: new AuthenticationService()
  };

  render() {
    const { services } = this;
    
    return (
      <div>TODO put application here</div>
    );
  }

}

ReactDOM.render(
  <App/>,
  document.querySelector('.application'));
